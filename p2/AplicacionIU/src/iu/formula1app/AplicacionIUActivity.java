package iu.formula1app;

import java.util.ArrayList;
import java.util.HashMap;

import likor.cafe.formula1app.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class AplicacionIUActivity extends Activity{
	
	private static final int DIALOG_CONNECTERR_ID = 0;
	public static String logTag = "AppF1";
    private ArrayList<HashMap<String, String>>  lista_adaptador;
    private	ListView list;
    private SimpleAdapter adaptadorPortrait1, adaptadorPortrait2, adaptadorLandscape1, adaptadorLandscape2,adaptadorLandscape3,adaptadorLandscape4;
    private ServicioCliente servicio;
	private AlertDialog alertDialog;
	private enum tipo_adaptador {PORTRAIT,LANDSCAPE1,LANDSCAPE2};
	private tipo_adaptador adaptador;
	
    /**
     *  Funcion que se llama cuando la actividad es creada por primera vez. 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);       
    		
		list = (ListView) findViewById(R.id.list);
		
		
		setAdapterType();
		
		
        list.setOnTouchListener(new View.OnTouchListener() {
        	
        	private float xini = 0;
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
			    switch(event.getAction()){
			    
			    	case MotionEvent.ACTION_DOWN:
			    		Log.d(AplicacionIUActivity.logTag, "Touch event -> Action = Down");
			    		xini = event.getX();
			    		break;
			    	
			    	case MotionEvent.ACTION_UP:
			    		Log.d(AplicacionIUActivity.logTag, "Touch event -> Action = Up");
			    		if (Math.abs(xini - event.getX()) > 30){
			    			switch (adaptador){
				    			case PORTRAIT:
				    				if(list.getAdapter() == AplicacionIUActivity.this.adaptadorPortrait2){
				    					list.setAdapter(AplicacionIUActivity.this.adaptadorPortrait1);
				    				}else{
				    					list.setAdapter(AplicacionIUActivity.this.adaptadorPortrait2);
				    				}
					    			break;
				    			case LANDSCAPE1:
				    				if(list.getAdapter() == AplicacionIUActivity.this.adaptadorLandscape2){
				    					list.setAdapter(AplicacionIUActivity.this.adaptadorLandscape1);
				    				}else{
				    					list.setAdapter(AplicacionIUActivity.this.adaptadorLandscape2);
				    				}
				    				break;
				    			case LANDSCAPE2:
				    				if(list.getAdapter() == AplicacionIUActivity.this.adaptadorLandscape3){
				    					list.setAdapter(AplicacionIUActivity.this.adaptadorLandscape4);
				    				}else{
				    					list.setAdapter(AplicacionIUActivity.this.adaptadorLandscape3);
				    				}
				    			break;
			    			}
			    		}

			    		break;
			    }
				    
				return false;
			}
		});           
    
        ServicioCliente.setIU(this);
        
        startService(new Intent(this, ServicioCliente.class));
        Log.d(logTag, "IniciadoServicio");
    
    }
    
    /**
     * Funcion que se vuelve a llamar al la Actividad.
     */     
    public void onResume(){
     
    	if( ! (servicio == null || this.servicio.conectado() || servicio.terminado())){
    		servicio.iniciar_conexion();
    	}
    	super.onResume();
    }
    
    /**
     * A�ade la opcion de acceder al menu de configuracion al pulsar la tecla MENU.
	 */
	@Override
 	public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, 0, 0, "Configuraci�n");
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Establece que al pulsar la opcion Configuracion del menu se abre la ventana de configuracion.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case 0:
				startActivity(new Intent(this, PreferencesActivity.class));
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

    /**
     *  Opcion que se llama al intentar crear un dialogo. Crea el dialogo que se
     *  muestra al producirse un error en la conexion.
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		if(id == AplicacionIUActivity.DIALOG_CONNECTERR_ID){
			if (this.alertDialog == null){
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(R.string.con_err_msg);
				builder.setPositiveButton("Reintentar",  new Dialog.OnClickListener(){

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						servicio.iniciar_conexion();
					}
				
				});
			
				builder.setNeutralButton("Configuraci�n", new Dialog.OnClickListener(){

					@Override
					public void onClick(DialogInterface dialog, int which) {
						AplicacionIUActivity.this.startActivity(new Intent(AplicacionIUActivity.this, PreferencesActivity.class));						
					}
				});
			
				builder.setNegativeButton("Salir", new Dialog.OnClickListener(){

					@Override
					public void onClick(DialogInterface dialog, int which) {
						AplicacionIUActivity.this.finish();				
					}
				});
			
				alertDialog = builder.create();
			}
			
			return alertDialog;
			
		}else{
			return super.onCreateDialog(id);		
		}
	}

	/**
	 * Establece el tipo de adaptador de la lista.
	 */
	private void setAdapterType(){
		int height = getWindowManager().getDefaultDisplay().getHeight();
        int width = getWindowManager().getDefaultDisplay().getWidth();
        
    	Log.d(logTag, "El ancho de pantalla es " + new Integer(width).toString());
        
        if(width > height){ // landscape
			Log.d(logTag, "Modo paisaje");
        	this.adaptador = width <= 320 ? tipo_adaptador.LANDSCAPE1 : tipo_adaptador.LANDSCAPE2 ;

        }else{ // portrait
			Log.d(logTag, "Modo retrato");
        	this.adaptador = tipo_adaptador.PORTRAIT;

        }
	}
	
	/**
	 * Funci�n que se llama cuando la configuraci�n (rotacion) del movil cambia.
	 */
	@Override
    public void onConfigurationChanged(Configuration _newConfig){
        super.onConfigurationChanged(_newConfig);
        setAdapterType();
        if(lista_adaptador != null){
			switch (adaptador){
			case PORTRAIT:
    			list.setAdapter(adaptadorPortrait1);
    			break;
			case LANDSCAPE1:
				Log.d(logTag, "Modo paisaje1");
				list.setAdapter(adaptadorLandscape1);
				break;
			case LANDSCAPE2:
				Log.d(logTag, "Modo paisaje2");
				list.setAdapter(adaptadorLandscape3);
				break;
			}
        }
    }
	
	/**
	 * Funcion que actualiza los datos del adaptador.
	 * @param datos
	 */
	public void updateIU(ArrayList<HashMap<String, String>> datos) {
		
		if (lista_adaptador == null){
			lista_adaptador = datos;
			
			adaptadorPortrait1 = new SimpleAdapter(this, this.lista_adaptador, R.layout.portrait1, 
	                new String[] {"pos", "short_name", "time_total","time_pred"}, new int[] {R.id.pos, R.id.name, R.id.time,R.id.prev});
			adaptadorPortrait2 = new SimpleAdapter(this , this.lista_adaptador, R.layout.portrait2, 
	                new String[] {"pos", "short_name", "sect1","sect2","sect3"}, new int[] {R.id.pos, R.id.name, R.id.psect,R.id.ssect,R.id.tsect});
	        adaptadorLandscape1 = new SimpleAdapter(this , this.lista_adaptador, R.layout.landscape1, 
	                new String[] {"pos", "short_name", "time_total", "sect1","sect2","sect3"}, new int[] {R.id.pos, R.id.name, R.id.time, R.id.psect,R.id.ssect,R.id.tsect});
	        adaptadorLandscape2 = new SimpleAdapter(this , this.lista_adaptador, R.layout.landscape2, 
	                new String[] {"pos", "short_name", "laps", "sect1","sect2","sect3"}, new int[] {R.id.pos, R.id.name, R.id.time, R.id.psect,R.id.ssect,R.id.tsect});
	        adaptadorLandscape3 = new SimpleAdapter(this , this.lista_adaptador, R.layout.landscape3, 
	                new String[] {"pos", "short_name", "time_total", "time_last", "sect1","sect2","sect3"}, new int[] {R.id.pos, R.id.name, R.id.time, R.id.prev, R.id.psect,R.id.ssect,R.id.tsect});
	        adaptadorLandscape4 = new SimpleAdapter(this , this.lista_adaptador, R.layout.landscape4, 
	                new String[] {"pos", "short_name", "laps", "time_last", "sect1","sect2","sect3"}, new int[] {R.id.pos, R.id.name, R.id.time, R.id.prev, R.id.psect,R.id.ssect,R.id.tsect});
	        
			switch (adaptador){
			case PORTRAIT:
    			list.setAdapter(adaptadorPortrait1);
    			break;
			case LANDSCAPE1:
				list.setAdapter(adaptadorLandscape1);
				break;
			case LANDSCAPE2:
				list.setAdapter(adaptadorLandscape3);
				break;
			}
		}
				
		((BaseAdapter) list.getAdapter()).notifyDataSetChanged();
		
	}

	/**
	 * Funcion a la que se llama cuando el servidor termina de enviar datos.
	 */
	public void finished() {
		Toast.makeText(this, "La carrera ha terminado", Toast.LENGTH_LONG);
	}

	/**
	 * Funcion a la que se llama cuando se ha producido un error en la conexi�n.
	 * @param servicioCliente
	 */
	public void conectionError(final ServicioCliente servicioCliente) {
		this.servicio = servicioCliente;
		this.showDialog(DIALOG_CONNECTERR_ID);
	}

	public void errorServidor() {
		Toast.makeText(this, "La conexi�n con el servidor ha terminado inesperadamente.", Toast.LENGTH_LONG);
	}	
}