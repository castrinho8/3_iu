package iu.formula1app;

import java.util.HashMap;

public class Piloto implements Comparable<Piloto> {

	private String shortName;
	private int position;
	private int lapTime, totalTime;
	private int laps;
	private int sect1time, sect2time, sect3time;
	private HashMap<String,String> map_adapter; 
	
	/**
	 * Constructor de la clase.
	 */
	public Piloto(String short_name, int pos) {
		this.shortName = short_name;
		this.position = pos;
		map_adapter = new HashMap<String,String>();
	}
	
	/**
	 * Funcion que devuelve el HashMap con sus datos.
	 */
	public HashMap<String,String> getData(Piloto anterior){
		map_adapter.clear();
		map_adapter.put("short_name", shortName);
		map_adapter.put("pos", ((Integer) position).toString());
		map_adapter.put("sect1", ((Integer) sect1time).toString());
		map_adapter.put("sect2", ((Integer) sect2time).toString());
		map_adapter.put("sect3", ((Integer) sect3time).toString());
		map_adapter.put("time_total", ((Integer) totalTime).toString());
		map_adapter.put("time_last", ((Integer) lapTime).toString());
		map_adapter.put("laps", ((Integer) laps).toString());
		map_adapter.put("time_pred", ((Integer) (totalTime - anterior.getTimeTotal())).toString());
		return map_adapter;
	}

	// Getters y Setters.
	public String getShortName() {
		return shortName;
	}

	public void setPosition(int pos) {
		this.position = pos;
	}
	
	public int getPosition(){
		return this.position;
	}

	public void setLapTime(int laps, int time_last, int time_total) {
		this.laps = laps;
		this.lapTime = time_last;
		this.totalTime = time_total;
	}
	
	public int getLaps(){
		return this.laps;
	}
	
	public int getTimeLast(){
		return this.lapTime;
	}
	
	public int getTimeTotal(){
		return this.totalTime;
	}

	public void setSectorTime(int sector, int time) {
		switch(sector){
		case 1:
			this.sect1time = time;
		case 2:
			this.sect2time = time;
		case 3:
			this.sect3time = time;
		}
	}
	
	public int getSectorTime(int sector){
		switch(sector){
		case 1:
			return this.sect1time;
		case 2:
			return this.sect2time;
		case 3:
			return this.sect3time;
		}
		return -1;
	}
	
	// Comparador
	@Override
	public int compareTo(Piloto another) {
		return ((Integer)this.position).compareTo(another.position);
	}

}
