/**
 * 
 */
package iu.formula1app;

import likor.cafe.formula1app.R;
import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Actividad de la vista de configuraci�n.
 */
public class PreferencesActivity extends PreferenceActivity {

	/**
	 * Funcion a la que se llama al crear la actividad.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferencias);
	
	}

}
