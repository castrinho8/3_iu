package iu.formula1app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

public class ServicioCliente extends Service {
	
    private static AplicacionIUActivity iu;
	
	private String IP;
	private int PORT;
    private ArrayList<HashMap<String, String>> lista_adaptador;
	private HashMap<String, String> headerTitles;
    private HashMap<String,Piloto> pilotos;
    protected Handler handler = new Handler();
	private ArrayList<Piloto> lista_pilotos;
	private boolean terminado, conectado;

	/**
	 * Constructor de la clase.
	 */
	public ServicioCliente(){
		lista_adaptador = new ArrayList<HashMap<String, String>>();
		lista_pilotos = new ArrayList<Piloto>();
		pilotos = new HashMap<String,Piloto>();
		conectado = true;
		terminado = false;
		
		headerTitles = new HashMap<String,String>();
		headerTitles.put("short_name", "Nombre");
		headerTitles.put("pos", "Pos.");
		headerTitles.put("sect1", "1� Sect.");
		headerTitles.put("sect2", "2� Sect");
		headerTitles.put("sect3", "3� Sect");
		headerTitles.put("time_total", "Tiempo");
		headerTitles.put("time_last", "�ltima");
		headerTitles.put("laps", "Vueltas");
		headerTitles.put("time_pred", "Diferencias");
		
		lista_adaptador.add(headerTitles);
		iu.updateIU(lista_adaptador);
		
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	/**
	 * Funcion a la que se llama cuando se inicia el servicio.
	 * @see android.app.Service#onStart(android.content.Intent, int)
	 */
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		this.iniciar_conexion();
	}

	/**
	 * Funcion que inicia un hilo que establece una conexion con el servidor.
	 */
	public void iniciar_conexion(){
		SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		this.PORT = Integer.parseInt(SP.getString("port_pref", "8000"));
		this.IP = SP.getString("ip_pref", "10.15.0.13");
		Log.d("ServicioF1Client", "Se inicia conexion con ip " + IP + " y puerto " + new Integer(PORT).toString() + ".");
		new HiloConexion().start();
	}
	
	/**
	 * Clase del hilo que inicia la conexion.
	 */
	public class HiloConexion extends Thread {

		private Socket socket;

		/** 
		 * Funcion que ejecuta el hilo.
		 */
		@Override
		public void run() {
			
			socket = new Socket();
			BufferedReader sEntrada = null;
						
			try {
				socket.connect(new InetSocketAddress(InetAddress.getByName(IP),PORT));
				Log.d("ServicioF1Cliente","Establecida conexion");
				conectado = true;
			} catch (IOException e1) {
				Log.e("ServicioF1Cliente","Error estableciendo conexion: " + e1.getLocalizedMessage());
				handler.post(new Runnable(){

					@Override
					public void run() {
						iu.conectionError(ServicioCliente.this);
						conectado = false;
					}	
				});
				
				return;
				
			}

			
			try {
				sEntrada = new BufferedReader(new InputStreamReader( socket.getInputStream()));
			} catch (IOException e) {
				Log.e("ServicioF1Cliente",e.getLocalizedMessage());
				try {
					socket.close();
				} catch (IOException e1) {
					Log.e("ServicioF1Cliente","Error estableciendo canal de entrada: " + e1.getLocalizedMessage());
				}
				return;
			}
			
			String linea;
			try {
				while((linea = sEntrada.readLine()) != null){
					final String tokens[] = linea.split(" ");
					
					if (tokens[0].equals("START_POS")){
						
						if(tokens.length != 3){
							Log.d("ServicioF1Cliente", "Mensaje erroneo: " + linea );
							continue;
						}
						
						handler.post(new Runnable(){

							@Override
							public void run() {
								try{
									ServicioCliente.this.add_driver(tokens[1], Integer.parseInt(tokens[2]));
								}catch (NumberFormatException n){
									
								}
							}
							
						});
						
					}else if (tokens[0].equals("SEC_TIME")){
						
						if(tokens.length != 5){
							Log.d("ServicioF1Cliente", "Mensaje erroneo: " + linea );
							continue;
						}
						
						handler.post(new Runnable(){

							@Override
							public void run() {
								try{	
									ServicioCliente.this.setSectorTime(tokens[1], Integer.parseInt(tokens[2]), Integer.parseInt(tokens[3]), 
															Integer.parseInt(tokens[4]));
								}catch (NumberFormatException n){

								}
							}
							
						});
						
					}else if (tokens[0].equals("LAP_TIME")){
					if(tokens.length != 6){
							Log.d("ServicioF1Cliente", "Mensaje erroneo: " + linea );
							continue;
						}
						
						handler.post(new Runnable(){

							@Override
							public void run() {
								try{
									ServicioCliente.this.setLapTime(tokens[1], Integer.parseInt(tokens[2]), Integer.parseInt(tokens[3]), Integer.parseInt(tokens[4]), Integer.parseInt(tokens[5]));
								}catch (NumberFormatException n){
									
								}
							}	
						});
						
					}else if (tokens[0].equals("FINISHED")){
						
						if(tokens.length != 1){
							Log.d("ServicioF1Cliente", "Mensaje erroneo: " + linea );
							continue;
						}
						
						handler.post(new Runnable(){

							@Override
							public void run() {
								terminado = true;
							}
							
						});
						
					}
				}
			} catch (IOException e) {
				Log.e("ServicioF1Cliente","Error leyendo linea: " + e.getLocalizedMessage());
				handler.post(new Runnable(){

					@Override
					public void run() {
						iu.errorServidor();
					}
					
				});
			}
			
			try {
				socket.close();
			} catch (IOException e) {
				Log.e("ServicioF1Cliente","Error saliendo del socket: " + e.getLocalizedMessage());
			}
		}
		
	}

	/**
	 * Funcion que a�ade un piloto a la lista.
	 * @param short_name Nombre corto del piloto
	 * @param pos Posicion inicial del piloto.
	 */
	public void add_driver(String short_name, int pos) {
		if (pilotos.get(short_name) != null){
			Log.e("ServicioF1Cliente","Error a�adiendo nuevo piloto: Ya existia un piloto con ese nombre.");
			return;
		}
		
		pilotos.put(short_name, new Piloto(short_name,pos));
		
	}

	/**
	 * Funcion que modifica el tiempo de un determinado sector.
	 * @param short_name Nombre corto del piloto.
	 * @param pos Posicion del piloto
	 * @param sector Numero de sector
	 * @param time Tiempo de ese sector.
	 */
	public void setSectorTime(String short_name, int pos, int sector, int time) {
		Piloto p = pilotos.get(short_name); 
		if (p == null){
			Log.e("ServicioF1Cliente","Error actualizando informacion de piloto: No existe un piloto con ese nombre.");
			return;
		}
		
		p.setPosition(pos);
		p.setSectorTime(sector,time);
		
	}

	/**
	 * Funcion que modifica el tiempo de vuelta de un piloto.
	 * @param short_name Nombre corto de un piloto
	 * @param pos Posicion del piloto en la carrera
	 * @param laps Vueltas que lleva el piloto
	 * @param time_last Tiempo de la ultima vuelta.
	 * @param time_total Tiempo total.
	 */
	public void setLapTime(String short_name, int pos, int laps, int time_last,
			int time_total) {
		Piloto p = pilotos.get(short_name); 
		if (p == null){
			Log.e("ServicioF1Cliente","Error actualizando informacion de piloto: No existe un piloto con ese nombre.");
			return;
		}
		
		p.setPosition(pos);
		p.setLapTime(laps,time_last,time_total);
		
		iu.updateIU(getData());
	}

	/**
	 * Funcion que crea el array de datos que se le pasa al adaptador.
	 * @return
	 */
	private ArrayList<HashMap<String, String>> getData(){
		lista_pilotos.clear();
		lista_pilotos.addAll(pilotos.values());
		Collections.sort(lista_pilotos);
		
		Piloto anterior = lista_pilotos.get(0);
		
		lista_adaptador.clear();
		
		lista_adaptador.add(headerTitles);
		
		for (Piloto p : lista_pilotos){
			lista_adaptador.add(p.getData(anterior));
			anterior = p;
		}
		
		return lista_adaptador;
	}

	/**
	 * Funcion que establece la actividad a la que se llama para hacer cambios en la interfaz grafica.
	 */
	public static void setIU(AplicacionIUActivity aplicacionIUActivity) {

		iu = aplicacionIUActivity;
	}

	/**
	 * Funcion que indica si se esta conectado al servidor.
	 */
	public boolean conectado(){
		return this.conectado;
	}
	
	/**
	 * Funcion que indica si el servidor a terminado de enviar datos (la carrera ha acabado).
	 */
	public boolean terminado(){
		return this.terminado;
	}

}
