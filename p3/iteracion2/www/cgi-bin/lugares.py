#!/usr/bin/python

import random

def random_alarm():
    rand_int = random.randint(0,3)
    if rand_int < 1:
        return -1
    elif rand_int < 2:
        return 0
    else:
        return 1


print "Content-Type: application/javascript"     # HTML is following
print                               # blank line, end of headers

print 'var alarma_casa_cocina  = new Alarma("Cocina",%i);' % random_alarm()
print 'var alarma_casa_garaje  = new  Alarma("Garaje",%i);' % random_alarm()
print 'var alarma_casa_robos   = new  Alarma("Robos",%i);' %  random_alarm()
print 'var alarma_comercio_humos = new  Alarma("Humos",%i);' % random_alarm()
print 'var alarma_comercio_inundacion = new  Alarma("Agua",%i);' % random_alarm()

print 'var casa = new  Lugar("Casa",[alarma_casa_cocina, alarma_casa_garaje,alarma_casa_robos],"../res/videos/fridge.webm","../res/images/casa.png");'
print 'var  comercio = new  Lugar("Comercio",[alarma_comercio_humos, alarma_comercio_inundacion],"../res/videos/fridge.webm", "../res/images/tienda.png");'

print 'lugares = [casa,comercio];'