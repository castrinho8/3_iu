
function removeChildren(a){
    while(a.childElementCount != 0){
        a.removeChild(a.children[0]);
    }
}

window.addEventListener('load',function() {

    var lista_lugares = document.getElementById("lugares");
    var tabla_alarmas = document.getElementById("tabla_alarmas");
    var video = document.getElementById("player");
    
    removeChildren(lista_lugares);

    for(var i=0; i< (lugares.length > 3 ? 3 : lugares.length); i++){
        lugar = lugares[i];
        item = new Image();
        item.setAttribute("class","img_lugar");
        item.setAttribute("src",lugar.imagen);
        item.setAttribute("height","100");
        item.setAttribute("width","100");
        item.setAttribute("alt",lugar.nombre);
        
        item.addEventListener('click', (function(lugar) { return function(e) {
            
            //Mostrar Alarmas.
            removeChildren(tabla_alarmas);

            for (var i = 0; i < lugar.alarmas.length; i++){
                alarma = lugar.alarmas[i];
                
                var tr = document.createElement("tr");
                
                var td_title = document.createElement("td");
                td_title.setAttribute("class","title_column");
                td_title.innerHTML = "<p>"+ alarma.nombre + "</p>\n";
                
                var td_image = document.createElement("td");
                td_image.setAttribute("class","image_column");
                                                
                var im = new Image();
                im.setAttribute("height","10");
                im.setAttribute("width","10");
                                
                if(alarma.estado == 1){
                    im.setAttribute("src","../res/images/boton_verde.png");
                    im.setAttribute("alt","Activada");
                }else if (alarma.estado == 0){
                    im.setAttribute("src","../res/images/boton_amarillo.png");
                    im.setAttribute("alt","Alerta");
                }else{
                    im.setAttribute("src","../res/images/boton_rojo.png");
                    im.setAttribute("alt","Desactivada");
                }
                
                td_image.appendChild(im);
                
                tr.appendChild(td_title);
                tr.appendChild(td_image);
                
                tabla_alarmas.appendChild(tr);
            }
            
            //Poner video.
            video.src = lugar.camerasrc;
			video.load();
			video.play();
			
			
		    };
		    
		    })(lugar),true);
		    
		lista_lugares.appendChild(item);
        if((i+1) != lugares.length){
           var sep = document.createElement("div");
          sep.setAttribute("class", "separador");
          lista_lugares.appendChild(sep);  
        } 
    }
},true);