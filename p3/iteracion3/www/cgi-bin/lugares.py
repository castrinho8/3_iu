#!/usr/bin/python
# -*- coding: utf8 -*-

import random
import json
import cgi

def random_alarm():
    rand_int = random.randint(0,3)
    if rand_int < 1:
        return -1
    elif rand_int < 2:
        return 0
    else:
        return 1
        
        
class Alarma:
    def __init__(self,name):
        self.name = name
        self.estado = random_alarm()
        
    def get_dict_repr(self):
        return {'class':'Alarma', 'nombre':self.name, 'estado':self.estado }
        
    def get_json(self):
        return json.dumps(self.get_dict_repr(), skipkeys=True)
        
    def update_alarm(self):
        self.estado = random_alarm()
        
class Lugar:
    def __init__(self,name,imagen,video,alarmas):
        self.name = name
        self.imagen = imagen
        self.video = video
        self.alarmas = alarmas
        
    def get_dict_repr(self):
        return {"class":"Lugar",
                "nombre":self.name, 
                "imagen":self.imagen, 
                "video":self.video,
                "alarmas":[a.get_dict_repr() for a in self.alarmas]}
                
    def get_json(self):
        return json.dumps(self.get_dict_repr(), skipkeys=True)

    def update_alarms(self):
        for a in self.alarmas:
            a.update_alarm()

alarma_casa_cocina = Alarma("Cocina");
alarma_casa_garaje = Alarma("Garaje");
alarma_casa_robos = Alarma("Robos");
alarma_comercio_humos = Alarma("Humos");
alarma_comercio_inundacion = Alarma("Agua");

casa = Lugar("Casa","res/images/casa.png","res/videos/fridge.webm",[alarma_casa_cocina, alarma_casa_garaje,alarma_casa_robos]);
comercio = Lugar("Comercio", "res/images/tienda.png","res/videos/fridge.webm",[alarma_comercio_humos, alarma_comercio_inundacion]);

form = cgi.FieldStorage()
lugar = form.getvalue("lugar")

print "Content-Type: application/json"
print

if lugar == "casa":
    print casa.get_json()
elif lugar == "comercio":
    print comercio.get_json()
else:
    print '[' + casa.get_json() + ',' +  comercio.get_json() + ']'