#! /usr/bin/env python

import sys
sys.path.append("./lib/")
sys.path.append("./src/")
from modelo import modelo
import os
import locale
import gettext

APP_NAME = "app"
LOCALE_DIR = os.path.abspath("./res/locale/")

gettext.textdomain(APP_NAME)
gettext.bindtextdomain(APP_NAME, LOCALE_DIR)
locale.setlocale(locale.LC_ALL)
locale.bindtextdomain(APP_NAME, LOCALE_DIR)

modelo.modelo().main()
