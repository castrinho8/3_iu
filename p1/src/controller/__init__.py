#COntroller
# -*- coding:utf-8 -*-

import gui
import modelo
import locale
import gettext
import gtk
import glib


class Controller:
    
    def __init__(self, model):
        self._ventana_activa = None
        self._gui = gui.gui(self)
        self._model = model
        
        self._ = gettext.gettext


    def iniciar_interfaz(self):
        """Funcion que inicia la interfaz"""
        self._gui.get_login_win()
        self._ventana_activa = 1
        glib.threads_init()
        gtk.gdk.threads_init()
        gtk.main()
        
    def login(self, user, passw):
        """Función de inicializacion y manejo del proceso de login"""
        glib.idle_add(self._gui.get_wait_win)
    
        res = self._model.login(user,modelo.md5(passw))
    
        glib.idle_add(self._gui.hide_wait_win)
        
        if res:
            self.profile()
        else:
            glib.idle_add(self._gui.get_err_win, self._("El nombre de usuario y/o contrasenha no son correctos. Intentelo de nuevo."))
    
    def profile(self):
        """Función de inicializacion para la ventana de perfil"""
        glib.idle_add(self._gui.get_wait_win)
        
        name = self._model.get_auten_name()
        desc = self._model.get_auten_desc()
        photo = self._model.get_auten_photo()
        l = self._model.get_auten_public()
        
        self._ventana_activa = 2
        
        glib.idle_add(self._gui.hide_wait_win)
        glib.idle_add(self._gui.hide_main)
        glib.idle_add(self._gui.get_profile_win, name, desc, photo, l)
           
    def friends(self):
        """Función de inicializacion para la ventana de amigos"""
        glib.idle_add(self._gui.get_wait_win)
        
        l = self._model.get_auten_friends() 

        self._ventana_activa = 3

        glib.idle_add(self._gui.hide_wait_win)
        glib.idle_add(self._gui.hide_main)
        glib.idle_add(self._gui.get_friends_win,l)
        
    def add_friend(self,user):
        """Función de inicializacion y manejo para añadir un amigo"""    
        glib.idle_add(self._gui.get_wait_win)

        r = self._model.add_friend(user)
        
        glib.idle_add(self._gui.hide_wait_win)
        
        if r == -1:
            glib.idle_add(self._gui.get_err_win, self._("Ya eres amigo de ") + user)
        elif r == 1:
            glib.idle_add(self._gui.get_err_win, self._("No existe ese usuario."))
        else:
            glib.idle_add(self._gui.hide_add_friend)
            if self._ventana_activa == 2:
                self.profile()
            elif self._ventana_activa == 3:
                self.friends()
                
    def del_friend(self,user):
        """Función de inicializacion y manejo para la eliminación de un amigo"""        
        glib.idle_add(self._gui.get_wait_win)
    
        r = self._model.del_friend(user)
        
        glib.idle_add(self._gui.hide_wait_win)

        if r == -1:
            glib.idle_add(self._gui.get_err_win, self._("No eres amigo de ") + user)
        elif r == 1:
            glib.idle_add(self._gui.get_err_win, self._("No existe ese usuario."))
        else:
            glib.idle_add(self._gui.hide_del_friend)
            if self._ventana_activa == 2:
                self.profile()
            elif self._ventana_activa == 3:
                self.friends()
                
            
    def mod_profile_win(self):
        """Función de inicializacion para la modificacion de perfil"""
        glib.idle_add(self._gui.get_wait_win)

        name = self._model.get_auten_name()
        desc = self._model.get_auten_desc()
        photo = self._model.get_auten_photo()
 
        glib.idle_add(self._gui.hide_wait_win)
        glib.idle_add(self._gui.get_edit_profile_win, name,desc,photo)
        
    def mod_profile(self, newname, newdesc, newphoto):
        """Función de manejo para la modificacion de perfil"""    
        glib.idle_add(self._gui.get_wait_win)    

        name = self._model.get_auten_name()
        desc = self._model.get_auten_desc()
        photo = self._model.get_auten_photo()

        name = (newname if name != newname else None)
        desc = (newdesc if desc != newdesc else None)
        photo = (newphoto if photo != newphoto else None)

        self._model.edit_auten(name,desc,photo)

        glib.idle_add(self._gui.hide_wait_win)

        if self._ventana_activa == 2:
            self.profile()

                
    def logout(self):
        """Función de manejo para el logout"""
        glib.idle_add(self._gui.get_wait_win)   
        
        self._model.logout()
        self._ventana_activa = 1
        
        glib.idle_add(self._gui.hide_wait_win)
        glib.idle_add(self._gui.hide_main)
        glib.idle_add(self._gui.get_login_win)
        
    def conf_win_create(self):
        """Función de inicializacion de la ventana de configuracion"""
        r = self._model.get_retard()

        glib.idle_add(self._gui.get_conf_win, r*100)
        
    def conf_win_accept(self, retard):
         """Función que setea el retardo"""
         self._model.set_retard(retard/100)

    def reg_profile(self,name, desc, nick, passw, photo):
        """Función de inicializacion y manejo de la ventana del registro de usuario"""
        glib.idle_add(self._gui.get_wait_win)   
        
        name = name if name else None
        desc = desc if desc else None
        nick = nick if nick else None
        passw = modelo.md5(passw) if passw else None
        photo = photo if photo else None
        
        glib.idle_add(self._gui.hide_wait_win)
        if not (name and nick and passw):
            glib.idle_add(self._gui.get_err_win, self._("No has introducido alguno de los campos obligatorios."))
        elif self._model.reg_user(name,desc,nick,passw,photo):
            glib.idle_add(self._gui.hide_reg_win)
        else:
            glib.idle_add(self._gui.get_err_win,self._("Ya existe un usuario con ese login."))
        
