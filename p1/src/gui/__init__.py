
# -*- coding:utf-8 -*-

import window_builder
import gtk

class gui:

    def __init__(self,controller):
        self._wb = window_builder.WindowBuilder(controller)
        
    def get_conf_win(self, retardo):
        """Funcion que muestra la ventana de configuracion"""
        self._wb.get_item("retard-scale").set_value(retardo)
        self._wb.get_item("conf-win").show_all()
    
    def hide_conf_win(self):
        """Funcion que esconde la ventana de configuracion"""
        self._wb.get_item("conf-win").hide()
        
    def get_login_win(self):
        """Funcion que muestra la ventana de login"""
        self._wb.get_item("login-win").show_all()
        
    def hide_login_win(self):
        """Funcion que esconde la ventana de login"""
        self._wb.get_item("login-win").hide()
    
    def get_err_win(self,text):
        """Funcion que muestra la ventana de error"""
        self._wb.get_item("err-msg").set_text(text)    
        self._wb.get_item("err-win").show_all()

    def hide_err_win(self):
        """Funcion que esconde la ventana de error"""
        self._wb.get_item("err-win").hide()

    def get_wait_win(self):
        """Funcion que muestra la ventana de espera"""
        self._wb.get_item("wait-win").show_all()
        
    def hide_wait_win(self):
        """Funcion que esconde la ventana de espera"""
        self._wb.get_item("wait-win").hide()
        
    def get_profile_win(self, name=None, desc=None, photo=None, publicaciones=None):
        """ Devuelve una ventana de perfil con las caracteristicas pasadas por 
        parametro. """
 
        self._wb.get_item("name-txt").set_text(name)  
        self._wb.get_item("desc-txt").set_text(desc)
 
        if photo:
            try:
                pixbuf = gtk.gdk.pixbuf_new_from_file(photo)
                self._wb.get_item("avatar1").set_from_pixbuf(pixbuf)
            except:
                pixbuf = gtk.gdk.pixbuf_new_from_file("res/images/avatar/noimage.png")
                self._wb.get_item("avatar1").set_from_pixbuf(pixbuf)
        
        model = self._wb.get_item("liststore-profile") 
        model.clear()

        for (date,user,publ) in publicaciones:
            model.append([date, user, publ])
            
        self._wb.get_item("profile-win").show_all()
     
    def hide_profile_win(self):
        """Funcion que esconde la ventana de perfil"""
        self._wb.get_item("profile-win").hide()
     
    def get_friends_win(self, amigos=None):
        """Funcion que muestra la ventana de amigos"""
        
        model = self._wb.get_item("liststore-amigos")
        model.clear()
        
        for (phot, user, desc) in amigos:
            try:
                pixbuf = gtk.gdk.pixbuf_new_from_file(phot)
            except:
                pixbuf = gtk.gdk.pixbuf_new_from_file("res/images/avatar/noimage.png")
                
            model.append([pixbuf, user, desc])
        
        self._wb.get_item("friends-win").show_all()
        
    def hide_friends_win(self):
        """Oculta la ventana de amigos."""
        self._wb.get_item("friends-win").hide()
        
    def get_edit_profile_win(self,name,desc,photo):
        """Funcion que muestra la ventana de editar perfil"""
    
        self._wb.get_item("edit-ent-name").set_text(name)
        self._wb.get_item("textbuffer-desc").set_text(desc)
        self._wb.get_item("edit-ent-photo").set_text(photo)

        self._wb.get_item("editprofile-win").show_all()
    
    def hide_edit_profile_win(self):
        """Oculta la ventana de editar perfil."""
        self._wb.get_item("editprofile-win").hide()    
    
    def get_reg_win(self):
        """Funcion que muestra la ventana de registro"""
        self._wb.get_item("textbuffer-desc").set_text("")
        self._wb.get_item("reg-win").show_all()
    
    def hide_reg_win(self):
        """Funcion que esconde la ventana de registro"""
        self._wb.get_item("reg-win").hide()
    
    def hide_main(self):
        """Funcion que esconde la ventana principal"""
        self._wb.get_item("friends-win").hide()
        self._wb.get_item("profile-win").hide()
        self._wb.get_item("login-win").hide()
        
    def get_add_friend(self):
        """Funcion que muestra la ventana de añadir amigo"""
        self._wb.get_item("addfriend-win").show_all()
        
    def hide_add_friend(self):
        """Funcion que esconde la ventana de añadir amigo"""
        self._wb.get_item("addfriend-win").hide()
    
    def get_del_friend(self):
        """Funcion que muestra la ventana de eliminar amigo"""
        self._wb.get_item("delfriend-win").show_all()    
        
    def hide_del_friend(self):
        """Funcion que esconde la ventana de eliminar amigo"""
        self._wb.get_item("delfriend-win").hide()
        
