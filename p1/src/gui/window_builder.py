# Clase constructora de la interfaz grafica.
# -*- coding:utf-8 -*-

import sys
import gtk
import gtk.glade
import pango
import threading
import gettext


APP_NAME = "app"

class WindowBuilder:

    def __init__(self,controller):
        """ Constructor. Inicializa las estructuras necesarias para la 
        construccion de las ventanas de la aplicación."""
        self._ = gettext.gettext        
        self._controller = controller
    
        self._glade = gtk.Builder()
        self._glade.set_translation_domain(APP_NAME) 
        self._glade.add_from_file("./res/layout/gui.glade")
        
        self._dic = {"login_ok_clicked" : self.login_ok_clicked,
            "exit_clicked" : self.exit_clicked,
            "about_clicked" : self.about_clicked,
            "conf_clicked" : self.conf_clicked,
            "end_program" : self.end_program,
            "hide_on_delete" : gtk.Widget.hide_on_delete,
            "hide_dialog" : self.hide_dialog,
            "view_friend_clicked" : self.view_friends_clicked,
            "view_profile_clicked": self.view_profile_clicked,
            "del_friend_clicked" : self.del_friends_clicked,
            "add_friend_clicked" : self.add_friends_clicked,
            "help_clicked" : self.help_clicked,
            "reg_clicked" : self.reg_clicked,
            "edit_profile" : self.edit_profile,
            "add_friend_ok" : self.add_friend_ok,
            "del_friend_ok" : self.del_friend_ok,
            "logout_clicked" : self.logout_clicked,
            "reg_ok_clicked" : self.reg_ok_clicked,
            "edit_ok_clicked": self.edit_ok_clicked,
            "conf_ok_clicked": self.conf_ok_clicked
            } 
    
        self._glade.connect_signals(self._dic)
        
        self.get_item("ayuda-login").set_sensitive(False)
        self.get_item("ayuda-perfil").set_sensitive(False)
        self.get_item("ayuda-amigos").set_sensitive(False)
        
        #Ventana de perfil
        treeview = self.get_item("treeview1")
        scroll = self.get_item("scroll-profile")
        model = self.get_item("liststore-profile")      
        
        treeview.append_column(gtk.TreeViewColumn(self._("Fecha"),gtk.CellRendererText(),text=0))
        treeview.append_column(gtk.TreeViewColumn(self._("Usuario"),gtk.CellRendererText(),text=1))
        
        render = gtk.CellRendererText()
        column = gtk.TreeViewColumn(self._('Contenido'), render, text = 2)
        treeview.append_column(column)
        scroll.connect_after('size-allocate', self.resize_treeview, treeview, column, render)
        self.label_set_autowrap(self.get_item("desc-txt"))
        
        #Ventana de amigos
        treeview = self.get_item("treeview2")
        scroll = self.get_item("scroll-friends")
        model = self.get_item("liststore-amigos")
        
        treeview.append_column(gtk.TreeViewColumn(self._("Avatar"), gtk.CellRendererPixbuf(), pixbuf=0))
        treeview.append_column(gtk.TreeViewColumn(self._("Nombre"), gtk.CellRendererText(), text=1))
        
        render = gtk.CellRendererText()
        column = gtk.TreeViewColumn(self._('Descripcion'), render, text = 2)
        treeview.append_column(column)
        scroll.connect_after('size-allocate', self.resize_treeview, treeview, column, render)
        

    def get_item(self, name):
        return self._glade.get_object(name)

        
    #----------------Handlers--------------------    
        
    #Ventana de login
        
    def login_ok_clicked(self, widget, data=None):
        """Manejador del evento producido al querer loguearse."""
        user =  self._glade.get_object("entry-user-login").get_text()
        passw = self._glade.get_object("entry-pass-login").get_text()
        threading.Thread(target=self._controller.login, args=(user,passw)).start()
        
    def logout_clicked(self,widget):
        """Manejador del evento de querer hacer logout. """
        threading.Thread(target=self._controller.logout, args=()).start()
        
        
    #Ventana de Acerca
        
    def about_clicked(self, widget, data=None):
        """ Manejador del evento de pulsar la opcion acerca que muestra 
        información sobre la aplicación"""
        self.get_item("acerca-win").show_all()
      
    #Ventan de COnfiguracion
      
    def conf_clicked(self, widget, data=None):
        """ Manejador del evento de pulsar la opcion de configuracion que 
        permite modificar el retardo de la aplicación.  """
        threading.Thread(target=self._controller.conf_win_create, args=()).start()
        
    def conf_ok_clicked(self, widget):
        """ Manejador del evento producido al aceptar unas determinadas configuraciones. """
        r = self.get_item("retard-scale").get_value()
        self._get_grandpa(widget).hide()
        threading.Thread(target=self._controller.conf_win_accept, args=(r,)).start()
        
    #Amigos anhadir,eliminar,ver
        
    def view_friends_clicked(self, widget):
        """ Manejador del evento producido al pulsar la opcion de ver amigos, 
        muestra la lista de amigos. """
        threading.Thread(target=self._controller.friends, args=()).start()

    def del_friends_clicked(self, widget):
        """Manejador del evento producido al pulsar la opcion de eliminar amigos"""
        self.get_item("delfriend-win").show_all()
       
    def del_friend_ok(self, widget):
        """Manejador del evento producido al aceptar la eliminacion de un amigo"""
        txt = self.get_item("del-friend-user").get_text()
        threading.Thread(target=self._controller.del_friend, args=(txt,)).start()
        
    def add_friends_clicked(self, widget):
        """Manejador del evento producido al pulsar la opcion de añadir un amigo"""
        self.get_item("addfriend-win").show_all()
        
    def add_friend_ok(self, widget):
        """Manejador del evento producido al aceptar el añadir un amigo"""
        txt = self.get_item("add-friend-user").get_text()
        threading.Thread(target=self._controller.add_friend, args=(txt,)).start()
    
    #Ventana de ayuda
    
    def help_clicked(self, widget):
        """  """
        pass
    
    #Perfil (ver,editar)
    
    def view_profile_clicked(self,widget):
        """Manejador del evento que se produce al pulsar en la opcion de ver perfil"""
        threading.Thread(target=self._controller.profile, args=()).start()
    
    def edit_profile(self,widget):
        """Manejador del evento que se produce al pulsar en la opcion de editar perfil"""
        threading.Thread(target=self._controller.mod_profile_win, args=()).start()
        
    def edit_ok_clicked(self,widget):
        """Manejador del evento que se produce al aceptar el editado del perfil"""
        (start, end) = self.get_item("textbuffer-desc").get_bounds()
        self.hide_dialog(widget)
        
        name = self.get_item("edit-ent-name").get_text()
        desc = self.get_item("textbuffer-desc").get_text(start, end, True)
        photo = self.get_item("edit-ent-photo").get_text()

        threading.Thread(target=self._controller.mod_profile, args=(name,desc,photo)).start()  
        
    #Registro
    
    def reg_clicked(self, widget):
        """Manejador del evento que se produce al pulsar en la opcion de registrar un nuevo usuario"""
        self.get_item("reg-win").show_all()
        
    def reg_ok_clicked(self, widget):
        """Manejador del evento que se produce al aceptar el registro de un nuevo usuario"""
        (start, end) = self.get_item("textbuffer-desc").get_bounds()
        name = self.get_item("reg-ent-name").get_text()
        desc = self.get_item("textbuffer-desc").get_text(start, end, True)
        nick = self.get_item("reg-ent-nick").get_text()
        passwd = self.get_item("reg-ent-pass").get_text()
        photo = self.get_item("reg-ent-photo").get_text()
        win = self._get_grandpa(widget)
        
        threading.Thread(target=self._controller.reg_profile, args=(name,desc,nick,passwd,photo)).start()
    

    #Comunes
    
    def hide_dialog(self, widget,data=None):
        """Funcion utilizada esconder un dialogo"""
        self._get_grandpa(widget).hide()
    
    def end_program(self, widget):
        """Funcion que cierra el programa."""
        gtk.main_quit()
        sys.exit()
             
    def exit_clicked(self, widget, data=None):
        """Funcion que muestra la ventana de comprobación de si realmente
        se desea salir del programa."""
        self.get_item("exit-win").show_all()
        return True
    
    #Resize...
    
    def resize_treeview(self, scroll, allocation, treeview, column, cell):
        """Funcion que redimensiona el treeview"""
        otherColumns = (c for c in treeview.get_columns() if c != column)
        newWidth = allocation.width - sum(c.get_width() for c in otherColumns)
        newWidth -= treeview.style_get_property("horizontal-separator") * 4
        if cell.props.wrap_width == newWidth or newWidth <= 0:
                return
        if newWidth < 300:
                newWidth = 300
        cell.props.wrap_width = newWidth
        column.set_property('min-width', newWidth + 10)
        column.set_property('max-width', newWidth + 10)
        store = treeview.get_model()
        iter = store.get_iter_first()
        while iter and store.iter_is_valid(iter):
                store.row_changed(store.get_path(iter), iter)
                iter = store.iter_next(iter)
                treeview.set_size_request(0,-1)
    
    def label_set_autowrap(self, widget): 
        "Make labels automatically re-wrap if their containers are resized.  Accepts label or container widgets."
        # For this to work the label in the glade file must be set to wrap on words.
        if isinstance(widget, gtk.Container):
            children = widget.get_children()
            for i in xrange(len(children)):
                self.label_set_autowrap(children[i])
        elif isinstance(widget, gtk.Label) and widget.get_line_wrap():
            widget.connect_after("size-allocate", self._label_size_allocate)


    def _label_size_allocate(self, widget, allocation):
        "Callback which re-allocates the size of a label."
        layout = widget.get_layout()
        lw_old, lh_old = layout.get_size()
        # fixed width labels
        if lw_old / pango.SCALE == allocation.width:
            return
        # set wrap width to the pango.Layout of the labels
        layout.set_width(allocation.width * pango.SCALE)
        lw, lh = layout.get_size()  # lw is unused.
        if lh_old != lh:
            widget.set_size_request(-1, lh / pango.SCALE)
            
    #Other func
            
    def _get_grandpa(self, widget):
    	"""Funcion que devuelve el padre de todos"""
        a = widget
        while a.get_parent():
            a = a.get_parent()
        return a
        
        
        
        
        
        
        
        
