#API simulada de una red social.
# -*- coding:utf-8 -*-


from hashlib import md5
import lipsum
import random
import datetime
import time

class RedSocialApi:
    """API simulada de una red social. La red social esta compuesta por un
    conjunto de usuarios que han establecido una relacion de amistad entre    
    ellos. Los usuarios tienen publicaciones que se organizan por fecha. Ademas
    en todas las operaciones se establece un retardo para simular que esta API
    esta trabajando a través de un servidor de internet. 
    El API de la red social no devuelve nunca objectos de la clase RedSocialUser
    sino que devuelve string con los datos de este objecto.
    Los datos de la red social no estan almacenados en ningun tipo de base de
    datos sino que se almacenan en memoria y al cerrar el programa se
    reinician."""
    
    def __init__(self):
        """Constructor de la clase. Inicializa hardcoded una serie de usuarios 
        de la red social."""
        self._retardo = 0
        self._usuarios = {}
        self._usuario_activo = None
        self._inicializar_red_social()   
            
    def get_retardo(self):
        """Getter para el retardo de la aplicación."""
        return self._retardo
        
        
    def set_retardo(self, retardo):
        """Setter para el retardo de la aplicación. """
        self._retardo = retardo
    
    def login(self, login, md5):
        """Metodo que intenta autenticar a un usuario en la API. Si el login a
         sido exitoso devuelve True en caso contrario, devuelve False."""
        self._retardar(3)
        luser = self._get_user(login)
        
        if luser and luser.check_md5(md5):
            self._usuario_activo = luser
            return True        
        return False
        
    def logout(self):
        """Hace logout del usuario actualmente autenticado en la API """
        self._retardar(3)
        self._usuario_activo = None
        
    def get_user_name(self, login=None):
        """Getter para el nombre de un determinado usuario. """
        self._retardar(3)
        self._comprobar_autenticado()
        
        try:
            return (self._get_user(login) if login else self._usuario_activo).get_name()
        except AttributeError:
            return None
        
    def set_user_name(self, name):
        """Setter para el nombre del usuario autenticado. """
        self._retardar(3)
        self._comprobar_autenticado()
        self._usuario_activo.set_name(name)
            
    def get_user_photo(self, login=None):
        """Getter para el path de la foto de un determinado usuario. """
        self._retardar(3)
        self._comprobar_autenticado()
        
        try:
            return (self._get_user(login) if login else self._usuario_activo).get_photo()
        except AttributeError:
            return None
        
    def set_user_photo(self, photo):
        """Setter para la imagen del usuario autenticado. """
        self._retardar(3)
        self._comprobar_autenticado()
        self._usuario_activo.set_photo(photo)
        
    def get_user_desc(self, login=None):
        """Getter para la descripcion de un determinado usuario. """
        self._retardar(3)
        self._comprobar_autenticado()
        luser = login if login else self._usuario_activo
    
        try:
            return (self._get_user(login) if login else self._usuario_activo).get_desc()
        except AttributeError:
            return None
        
    def set_user_desc(self, desc):
        """Setter para la descripcion del usuario autenticado. """
        self._retardar(3)
        self._comprobar_autenticado()
        self._usuario_activo.set_desc(desc)
        
    def get_amigos(self):
        """Getter para los amigos del usuario autenticado. """
        self._retardar(3)
        self._comprobar_autenticado()
        return [self._get_user(a).get_login() for a in 
                self._usuario_activo.get_amigos()]
                
    def anhadir_amigo(self, login):
        """Método que permite añadir un amigo al usuario autenticado. """
        self._retardar(3)        
        self._comprobar_autenticado()
        
        u = self._get_user(login)
        if u:
            self._usuario_activo.add_amigo(u)
            return 0 
        else:
            return 1 #No existe el usuario

            
    def eliminar_amigo(self, login):
        """Método que permite eliminar un amigo del usuario autenticado. """
        self._retardar(3)        
        self._comprobar_autenticado()
        
        u = self._get_user(login)
        if u:
            self._usuario_activo.eliminar_amigo(u)
            return 0
        else:
            return 1 #No existe el usuario
        
    def get_publicaciones(self, login=None):
        """ Metodo que permite ver las publicaciones de un amigo del usuario
         autenticado. Devuelve una lista de tuplas (Fecha de publicacion,
          Contenido) """
        self._retardar(3)
        self._comprobar_autenticado() 
         
        luser = (self._get_user(login) if login else self._usuario_activo)
          
        return [(d,luser.get_todas_publicaciones()[d]) 
                for d in luser.get_todas_publicaciones()]
    
    def registrar_user(self, login, md5, name, desc=None, photo=None):
        """Método que permite registrar un nuevo usuario en la red social.
         Devuelve False si ya existia un usuario con ese login en la red 
         social."""
        self._retardar(3)
        if not self._usuarios.has_key(login):
            self._usuarios[login] = _RedSocialUser(login,name,md5,desc,photo)
            return True
        return False
    
    @staticmethod
    def md5(s):
        """Devuelve el hash md5 del String pasado por parametro."""
        return md5(s).hexdigest()
    
    @staticmethod
    def _random_paragraph():
        """Devuelve un parrafo con contenido generado por el modulo lipsum."""
        return lipsum.Generator().generate_paragraph()
    @staticmethod    
    def _random_date():
        """Devuelve una fecha aleatoria del año 2011."""
        day = int(random.uniform(1,27))
        month = int(random.uniform(1,12))
        year = 2011
        hour = int(random.uniform(0,23))
        return datetime.datetime(year,month,day,hour,0,0)

    def _retardar(self,a):
        """ Duerme la aplicacion un tiempo que depende del valor pasado por 
        parametro y de la propiedad cambiable con get/set_retardo. """
        n = int(random.uniform(0,a))
        time.sleep(a*self.get_retardo())
            
    
    def _get_user(self, login):
        """Devuelve el objecto usuario correspondiente al nombre de usuario 
        pasado por parametro o None en caso de no existir."""
        try:
            return self._usuarios[login] 
        except:
            return None
    def _comprobar_autenticado(self):
        """Comprueba si hay aguien autenticado"""
        if not self._usuario_activo:
            raise Exception("Nadie autenticado")
         
         
    def _publicaciones_aleatorias(self,user):
    	"""Crea un numero aleatorio entre 1 y 20 de publicaciones
    	para un usuario."""
        n = int(random.uniform(0,10))
        
        for i in range(0,n):
            user.add_publicacion(RedSocialApi._random_date(), RedSocialApi._random_paragraph()*int(random.uniform(1,2)))
     
    def _inicializar_red_social(self):
        """Inicializa los datos de la base de datos."""
        #Usuarios
        pablo = _RedSocialUser("pablo",
                             "Pablo Diaz",
                              RedSocialApi.md5("pablo"),
                              RedSocialApi._random_paragraph()*2,
                              "res/images/avatar/5.png")
        pedro = _RedSocialUser("pedro",
                             "Pedro Castro",
                              RedSocialApi.md5("pedro"),
                              RedSocialApi._random_paragraph(),
                              "res/images/avatar/2.png")
        antonio = _RedSocialUser("antonio",
                             "Antonio Franco",
                              RedSocialApi.md5("antonio"),
                              photo="res/images/avatar/7.png")
        guillermo = _RedSocialUser("guillermo",
                             "Guillermo Teijeiro",
                              RedSocialApi.md5("pablo"),
                              RedSocialApi._random_paragraph()*2)
        sara = _RedSocialUser("sara","Sara Rodriguez", RedSocialApi.md5("sara"))
        
        #Amistades
        pablo.add_amigo(pedro)
        pablo.add_amigo(sara)
        pedro.add_amigo(sara)
        pedro.add_amigo(guillermo)
        pedro.add_amigo(antonio)
        antonio.add_amigo(pablo)
        antonio.add_amigo(pedro)
        antonio.add_amigo(sara)
        guillermo.add_amigo(pablo)
        sara.add_amigo(pablo)
        sara.add_amigo(pedro)
                
        #Publicaciones
        self._publicaciones_aleatorias(pablo)
        self._publicaciones_aleatorias(pedro)            
        self._publicaciones_aleatorias(sara)
        self._publicaciones_aleatorias(antonio)
        self._publicaciones_aleatorias(guillermo) 
                        
        #Se agregan los usuarios a la red social.
        self._usuarios["pablo"] = pablo
        self._usuarios["pedro"] = pedro
        self._usuarios["antonio"] = antonio
        self._usuarios["guillermo"] = guillermo
        self._usuarios["sara"] = sara


class _RedSocialUser():
    """Clase que identifica a un usuario de la red social."""
    
    def __init__(self, login, name, md5, desc=None, photo=None):
        """Constructor de la clase. Recibe login (el login del usuario), 
        name(el nombre del usuario), md5 (el hash de la password del usuario)
         y, de forma opcional, una descripción y/o una foto del mismo."""
        self._login = login
        self._name = name
        self._md5 = md5
        self._desc = (desc if desc else "")
        self._photo = (photo if photo else "")
        self._publicaciones = {}
        self._amigos = {}
       
    #Getters y Setters 
        
    def get_login(self):
        """Getter para el login del usuario."""
        return self._login
        
    def get_name(self):
        """Getter para el nombre del usuario."""
        return self._name
    
    def set_name(self,name):
        """Setter para el nombre de usuario."""    
        self._name = name
    
    def get_desc(self):
        """Getter para la descripcion del usuario."""
        return self._desc
        
    def set_desc(self, desc):
        """Setter para la descripcion del usuario."""
        self._desc = desc
        
    def get_photo(self):
        """Getter para el path de la foto del usuario."""
        return self._photo
    
    def set_photo(self, photopath):
        """Setter para la foto"""
        self._photo = photopath
    
    def check_md5(self,md5):
        """Comprueba si el hash MD5 pasado por parametro se corresponde son el
         del usuario."""
        return self._md5 == md5
    
    #Publicaciones        
        
    def get_publicacion(self,date):
        """Getter para una de las publicaciones del usuario."""
        return self._publicaciones[date]
    
    def get_todas_publicaciones(self):
        """Getter para todas las publicaciones de un usuario."""
        return self._publicaciones

    def add_publicacion(self,date,text):
        """Añade una publicación a la lista de publicaciones del usuario."""
        if not self._publicaciones.has_key(date):
            self._publicaciones[date] = text
    
    #Amigos

    def get_amigos(self):
        """Getter para todos los amigos del usuario."""
        return self._amigos
        
    def add_amigo(self, amigo):
        """Añade un amigo."""
        if self._amigos.has_key(amigo.get_login()):
            return -1
        else:
            self._amigos[amigo.get_login()] = amigo
            return 0
        
    def eliminar_amigo(self, amigo):
    	"""Elimina un amigo"""
        if self._amigos.has_key(amigo.get_login()):
            self._amigos.__delitem__(amigo.get_login())
            return 0
        else:
            return -1
            
