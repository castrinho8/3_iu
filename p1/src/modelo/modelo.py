#Modelo
# -*- coding:utf-8 -*-

import API
from controller import Controller


class modelo:
    
    def __init__(self):
        self._api = API.RedSocialApi()
        self._controller = Controller(self)
        
    def main(self):
        self._controller.iniciar_interfaz()
        
    def login(self, user, passw):
    	"""Llama al metodo de login de la API"""
        return self._api.login(user,passw)
    
    def logout(self):
    	"""Llama al logout de la API"""
        self._api.logout()
        
    def get_auten_name(self):
    	"""Llama a get user en la API"""
        return self._api.get_user_name()
    
    def get_auten_desc(self):
    	"""Llama al getter de la descripcion de usuario"""
        return self._api.get_user_desc() 
    
    def get_auten_photo(self):
    	"""Llama al getter de la foto de usuario"""
        return self._api.get_user_photo()
    
    def get_auten_public(self):
        """Devuelve una lista con las publicaciones de todos los amigos"""
        l = []
        
        for a in self._api.get_amigos(): 
            for (d,p) in self._api.get_publicaciones(a):
                l.append((d.__str__(),a,p))
        return l
        
        
    def get_auten_friends(self):
    	"""Devuelve los datos de todos los amigos"""
        return [(self._api.get_user_photo(a),a,self._api.get_user_desc(a)) for a 
                in self._api.get_amigos()]
        
    def edit_auten(self, name=None, desc=None, photo=None):
    	"""Edita los datos del usuario"""
        if name:
            self._api.set_user_name(name)
        
        if desc:
            self._api.set_user_desc(desc)
            
        if photo:
            self._api.set_user_photo(photo)
            
        
    def add_friend(self,login):
    	"""Comprueba si ya tiene al amigo indicado y sino lo añade"""
        if login in self._api.get_amigos():
            return -1
            
        return self._api.anhadir_amigo(login)
            
    def del_friend(self, login):
    	"""Comprueba si tiene al amigo indicado y lo elimina"""
        if not (login in self._api.get_amigos()):
            return -1
        
        return self._api.eliminar_amigo(login)
    
    def set_retard(self,a):
    	"""Setea el retardo"""
        self._api.set_retardo(a)
        
    def get_retard(self):
    	"""Devuelve el retardo establecido"""
        return self._api.get_retardo()
        
    def reg_user(self,name,desc,nick,passw,photo):
    	"""Registra a un nuevo usuario"""
        return self._api.registrar_user(nick,passw,name,desc,photo)
        
        
